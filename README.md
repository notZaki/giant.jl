# GIANT.jl

GIANT Is An NMF Toolbox.

It is a collection of algorithms to solve different variants of Nonnegative Least Squares (NNLS) problems and Nonnegative Matrix Factorization (NMF).
In particular, it focuses on **Sparse NNLS/NMF**, the main topic of my work.

It is still a work in progress, with lots of inconstitencies, small bugs, and badly written code.
If you have any problem or question when using GIANT, please [contact me](http://nicolasnadisic.xyz/) and I will be happy to help!

GIANT is free software, licensed under the GNU GPL v3.

## A word on NNLS and NMF

### NMF

NMF consists in the following optimization problem.
Given a data matrix  $`M \in \mathbb{R}^{m \times n}`$ and a factorization rank $`r`$, we compute the factors $`W \in \mathbb{R}^{m \times r}_{+}`$ and $`H \in \mathbb{R}^{r \times n}_{+}`$ such that $`M \approx WH`$.
In standard NMF, when using the Frobenius norm as a data fidelity measure, this reduces to
$`\min\limits_{H \geq 0, W \geq 0} \frac{1}{2} \| M - WH \|_F^2`$.

### MNNLS

NMF problems are usually solved by alternating between the optimization of $`W`$ and $`H`$, by iteratively updating one factor while fixing the other.
This leads to a NNLS subproblem with multiple right-hand sides (MNNLS), that is $`\min\limits_{H \geq 0} \frac{1}{2} \| M - WH \|_F^2`$ for the update of $`H`$ (and similarly for $`W`$).

### NNLS

An MNNLS problem can be decomposed into $`n`$ independent NNLS subproblems of the form
$`\min\limits_{x \geq 0} \frac{1}{2} \| Ax - b \|_2^2`$
where $`H(:,j)`$, $`W`$, and $`M(:,j)`$ correspond respectively to $`x`$, $`A`$, and $`b`$.
Many algorithms for NMF are thus based on NNLS algorithms.


## Getting started

Once cloned, enter the project's folder and start Julia.

Type `]` to enter Pkg mode, then type `activate .` and then `instantiate` to install the necessary packages. Then type backspace to leave Pkg mode.

To run a test:
`julia> include("test/<nameoftest>.jl")`

The first run will be slow, as everything needs to be compiled.
For performance measures, always run scripts twice.

The datasets present in this repository come from [this website](http://lesun.weebly.com/hyperspectral-data-set.html).
They are stored here using Git LFS.
Make sure to [install Git LFS](https://git-lfs.github.com/) if you want to download them via Git.
You can also download them via the [web interface](https://gitlab.com/nnadisic/giant.jl/-/tree/master/xp/data).

## Algorithms

### NNLS
- HALS (hierarchical alternating least squares) is a standard algorithm for MNNLS, based on block-wise coordinate descent. It's one the most used approaches for NMF.
- NNLS is an active-set method to solve NNLS problems exactly.

### Sparse NNLS
- arborescent (k-sparse NNLS)
- Homotopy

### Separable NMF
- SNPA
- Brassens (Sparse Separable NMF)

### Other
- Hyperspectral
- Util



## Experiments

### Homotopy

To reproduce the experiments from the paper "A Homotopy-based Algorithm for Sparse Multiple Right-hand Sides Nonnegative Least Squares", by N. Nadisic et al, use the script `xp/homotopy/runxp.jl`.

For comparison purposes, we also include the Matlab code for NNOLS, by Nguyen et al ([link to their paper](https://hal.archives-ouvertes.fr/hal-02049424/document)).
Their code was released as a free software.
Here you will find the version we used in our experiments, but a more recent one may be available in [the authors' repository](https://codeocean.com/capsule/1591546/tree/v1).


## Known issues

### SingularException

When using the `activeset` function from `nnls.jl`, if the matrix `AtA` is very ill-conditionned or near-singular, Julia may stop the execution and throw the error "SingularException".
This is notably the case with Brassens, which uses heavily `activeset` for computations that are likely to be very ill-conditioned.
While this makes senses, as the results of such computation can be very inexact, you may prefer to have a bad results rather than no result at all.
This behaviour actually originates from the BLAS library.
In my case, the default one was OpenBLAS and I did not find how to change this behaviour.
A workaround that worked for me is to use [MKL for Julia](https://github.com/JuliaLinearAlgebra/MKL.jl) instead of OpenBLAS.
