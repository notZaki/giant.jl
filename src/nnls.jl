# NNLS solver

export reducednnls, activeset, matrixactiveset


# From the paper "Towards faster NMF..." Kim and Park 2008
function activeset(AtA::AbstractMatrix{T},
                   Atb::AbstractVector{T},
                   x0::AbstractVector{T}=Vector{T}(undef, 0);
                   sumtoone::Bool=false) where T <: Real
    # Init constants
    r = size(AtA, 1)
    maxiter = 5*r
    tol = 1e-12

    # Init variables
    t = r + 1
    p = 3

    # If not provided, init x full of zeros
    x = length(x0) == 0 ? zeros(r) : x0
    y = zeros(r)

    # Init support (what Kim calls passive set, ie, entries not constrained to 0)
    F = x .> tol

    # NNLS main loop
    for it in 1:maxiter
        # Solve least squares (x) and compute gradient (y)
        if !sumtoone # standard algorithm
            x[F] = AtA[F,F] \ Atb[F]
            y[.~F] = AtA[.~F,F] * x[F] - Atb[.~F]
        else # modified so that solution has unit l1 norm
            sF = sum(F)
            As = [AtA[F,F] -ones(sF,1) ; ones(1,sF) 0]
            bs = [Atb[F] ; 1]
            sol = As \ bs
            x[F] = sol[1:sF]
            mu = sol[sF+1]
            y[.~F] = AtA[.~F,F] * x[F] - Atb[.~F] .- mu
        end
        x[.~F] .= 0
        y[F] .= 0

        # Compute h1 and h2 (optimality conditions)
        h1 = (x .< -tol) .& F
        h2 = (y .< -tol) .& .~F
        h1c = falses(r)
        h2c = falses(r)

        notgood = sum(h1) + sum(h2)

        # Stopping criterion (if optimality reached)
        if notgood <= 0
            break
        end

        # Update
        if notgood < t
            t = notgood
            p = 3
            h1c = copy(h1)
            h2c = copy(h2)
        end

        if (notgood >= t) & (p >= 1)
            p = p - 1
            h1c = copy(h1)
            h2c = copy(h2)
        end

        if (notgood >= t) & (p == 0)
            tochange = findlast(h1 .& h2)
            if tochange!=nothing && h1[tochange]
                h1c .= false
                h1c[tochange] .= true
                h2c .= false
            else
                h1c .= false
                h2c .= false
                if tochange!=nothing
                    h2c[tochange] .= true
                end
            end
        end

        F = xor.(F, h1c) .| h2c
    end
    # Remove numeric noise and return
    x[x .< tol] .= 0
    return x
end


# Solve NNLS reduced to a given support
function reducednnls(AtA, Atb, supp, x0; sumtoone=false)
    x = zeros(size(AtA, 1))
    x[supp] = activeset(AtA[supp, supp], Atb[supp], x0[supp],
                        sumtoone=sumtoone)
    resid = norm(AtA * x - Atb)
    return x, resid
end


# Solve multiple right-hand side NNLS
function matrixactiveset(W::AbstractMatrix{T},
                         M::AbstractMatrix{T},
                         H0::AbstractMatrix{T}=Matrix{T}(undef, 0, 0);
                         inputgrammat::Bool=false,
                         sumtoone::Bool=false) where T <: Real
    # If not provided, precompute Gram matrices
    WtW = inputgrammat ? W : W' * W
    WtM = inputgrammat ? M : W' * M
    # Init constants
    r, n = size(WtM)
    # If not provided, init H full of zeros
    H = length(H0) == 0 ? zeros(r, n) : H0
    # Solve the n NNLS subproblems with activeset
    for j in 1:n
        H[:,j] .= activeset(WtW, WtM[:,j], H[:,j], sumtoone=sumtoone)
    end
    # Return
    return H
end
