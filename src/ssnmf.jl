# Sparse Separable NMF, algorithm Brassens from paper http://nicolasnadisic.xyz/publication/ssnmf/
# Given a matrix M, find W = M(:,J) and H col-wise k-sparse such that M ~= W * H

export brassens


function brassens(M::AbstractMatrix{T},
                  k::Integer,
                  rextmax::Integer;
                  deltastop::T=1e-16,
                  normalize::Bool=false) where T <: Real

    # If necessary, normalize columns so that they sum to one
    if normalize
        for col in eachcol(M)
            if sum(col) > 0
                col ./= sum(col)
            end
        end
    end

    # Extract the exterior vertices with classic SNPA
    extJ, _ = snpa(M, rextmax, deltastop=deltastop)
    # Extract the candidate interior vertices with kSSNPA
    candJ = kssnpa(M, k, extJ, deltastop=deltastop)
    # Remove exterior vertices from candidates
    candJ = setdiff!(candJ, extJ)
    nbcand = length(candJ)

    # Discard bad candidates, that is, candidates j such that
    # min_{h k-sparse} || M(:,j) - M(:,candJ\{j}) * h ||_2^2 > 0
    J = copy(extJ)
    for i in 1:nbcand
        tmpW = M[:, setdiff(vcat(extJ, candJ), candJ[i])]
        tmpH, _ = arborescent(tmpW'*tmpW, tmpW'*M[:,candJ[i]], k, sumtoone=true)

        if norm(M[:,candJ[i]] - tmpW*tmpH) / norm(M) > deltastop
            push!(J, candJ[i])
        end
    end

    # Compute final H
    H, _ = ksparse_mnnls(M, M[:,J], k, sumtoone=true)

    return J, H, extJ, candJ
end


# SNPA algorithm where the projection step has been changed to a k-sparse projection
function kssnpa(M::AbstractMatrix{T},
                k::Integer,
                extJ::AbstractVector{Integer};
                deltastop::T=1e-16) where T <: Real

    # Init
    m, n = size(M)
    J = copy(extJ) # indices of selected columns, init with exterior vertices
    it = 1

    # Compute initial k-sparse H
    H0, _ = ksparse_mnnls(M, M[:,J], k, sumtoone=true)

    # Init residual and compute L2 norms of its columns
    R = M - M[:,J]*H0
    norms_colsR = [norm(col)^2 for col in eachcol(R)]
    norms_colsM = [norm(col)^2 for col in eachcol(M)]
    maxnm = maximum(norms_colsM)

    # Loop to find candidates until relative reconstruction error is small enough
    while maximum(norms_colsR) / maxnm > deltastop
        # Select col of R with biggest L2-norm
        maxresid, idxmaxresid = findmax(norms_colsR)
        # Update the index set
        push!(J, idxmaxresid)

        # Project residual
        H = rand(it, n)
        if it == 1
            # Compute with arborescent (col-wise k-sparse) with sum-to-one constraint
            H, _ = ksparse_mnnls(M, M[:,J], k, sumtoone=true)
        else
            # H[:,J[it]] .= 0
            # h = zeros(n)
            # h[J[it]] = 1
            # H[it,:] .= h
            # # Update H in-place
            # # TODO for the moment, arborescent ignores the H given in input, we have to use it to initialize the active set
            # ksparse_updtH!(M, M[:,J], H, k, sumtoone=true)
            # TMP
            H, _ = ksparse_mnnls(M, M[:,J], k, sumtoone=true)
        end
        R = M - M[:,J] * H
        retH = copy(H)

        # Update norms
        norms_colsR = [norm(col)^2 for col in eachcol(R)]

        it += 1
    end

    return J
end
