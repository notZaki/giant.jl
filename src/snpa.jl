# Successive nonnegative projection algorithm, for Separable NMF
# From the paper "Successive Nonnegative Projection Algorithm for Robust
# Nonnegative Blind Source Separation" by Nicolas Gillis

export snpa


function snpa(M::AbstractMatrix{T},
              r::Integer;
              deltastop::T=1e-16,
              normalize::Bool=false) where T <: Real
    # Init
    m, n = size(M)
    J = Integer[] # indices of selected columns

    # Prepare return
    retH = Matrix

    # Normalize columns so that they sum to one
    if normalize
        for col in eachcol(M)
            if sum(col) > 0
                col ./= sum(col)
            end
        end
    end

    # Init residual and compute L2 norms of its columns
    R = copy(M)
    norms_colsR = [norm(col)^2 for col in eachcol(R)]
    maxnm = maximum(norms_colsR)

    # Loop, maximum r iterations
    for it in 1:r
        # Break if relative reconstruction error is small enough
        if maximum(norms_colsR) / maxnm < deltastop
            break
        end
        # Select col of R with biggest L2-norm
        maxresid, idxmaxresid = findmax(norms_colsR)
        # Handle ties
        # TODO: see NG matlab code
        # Update the index set
        push!(J, idxmaxresid)

        # Project residual
        H = rand(it, n)
        if it == 1
            # Compute with active-set with sum-to-one constraint
            H = matrixactiveset(M[:,J], M, sumtoone=true)
        else
            H[:,J[it]] .= 0
            h = zeros(n)
            h[J[it]] = 1
            H[it,:] .= h
            H = matrixactiveset(M[:,J], M, H, sumtoone=true)
        end
        R = M - M[:,J] * H
        retH = copy(H)

        # Update norms
        norms_colsR = [norm(col)^2 for col in eachcol(R)]
    end

    return J, retH
end
