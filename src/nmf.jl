export nmf

function nmf(M::AbstractMatrix{T}, r::Integer;
             maxiter::Integer=100,
             W0::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
             H0::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
             updaterW!::Function=halsW!,
             updaterH!::Function=halsH!) where T <: Real

    # Constants
    m, n = size(M)

    # If not provided, init W and H randomly
    W0 = length(W0) == 0 ? rand(m, r) : W0
    W = copy(W0)
    H0 = length(H0) == 0 ? rand(r, n) : H0
    H = copy(H0)

    # Init result vectors
    times = Array{Float64}(undef, maxiter + 1)
    times[1] = 0
    errors = Array{Float64}(undef, maxiter + 1)
    errors[1] = relreconstructionerror(M, W, H)

    # Alternate optimization
    for it in 1:maxiter
        time = @elapsed begin
            updaterW!(M, W, H)
            updaterH!(M, W, H)
        end
        times[it+1] = times[it] + time
        errors[it+1] = relreconstructionerror(M, W, H)
    end

    return W, H, times, errors
end
