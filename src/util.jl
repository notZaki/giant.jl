# Utilities for GIANT

export relreconstructionerror, colsparsity, rand_ksparse, rand_ksparse_vect,
    rand_deltaksparse_vect, rand_deltaksparse

using Random, LinearAlgebra


"""
    relreconstructionerror(M, W, H)

Returns the relative reconstruction error of the factorization, in terms of the
Frobenius norm.
"""
function relreconstructionerror(M, W, H)
    return norm(M - W*H) / norm(M)
end


"""
    colsparsity(mat, epsilon=1e-8)

Returns the average column-wise sparsity of the given matrix, where sparsity
is defined as the number of entries greater than epsilon.
"""
function colsparsity(mat, epsilon=1e-3)
    normmat = copy(mat)
    for col in eachcol(normmat)
        col ./= maximum(col)
    end
    return count(x -> x > epsilon, normmat) / size(normmat, 2)
end


"""
    rand_ksparse(m, n, k)

Generates a random matrix of size m*n with exactly k nonzero entries by column.
"""
function rand_ksparse(m, n, k)
    mat = zeros(m, n)
    for col in eachcol(mat)
        col .= rand_ksparse_vect(m, k)
    end
    return mat
end


"""
    rand_ksparse_vect(n, k)

Generates a random vector of size n with exactly k nonzero entries.
"""
function rand_ksparse_vect(n, k)
    k <= n || error("k is greater than n")
    vect = zeros(n)
    perm = randperm(n)[1:k]
    for i in perm
        vect[i] = rand()
    end
    return vect
end


"""
    rand_deltaksparse(m, n, k, delta)

Generates a random matrix of size m*n with k+d nonzero entries by column, where d is a
random integer between -delta and delta.
"""
function rand_deltaksparse(m, n, k, delta)
    mat = zeros(m, n)
    for col in eachcol(mat)
        col .= rand_deltaksparse_vect(m, k, delta)
    end
    return mat
end


"""
    rand_deltaksparse_vect(n, k, delta)

Generates a random vector of size n with k+d nonzero entries, where d is a random
integer between -delta and delta.
"""
function rand_deltaksparse_vect(n, k, delta)
    k <= n || error("k is greater than n")
    newk = k + rand(-delta:delta)
    newk = min(newk, n)
    vect = zeros(n)
    perm = randperm(n)[1:newk]
    for i in perm
        vect[i] = rand()
    end
    return vect
end
