module GIANT

include("nmf.jl")
include("nnls.jl")
include("hals.jl")
include("ksparsennls.jl")
include("homotopy.jl")
include("pathassignement.jl")
include("ksparsematrix.jl")
include("homotopymatrix.jl")
include("snpa.jl")
include("ssnmf.jl")
include("util.jl")
include("hyperspectral.jl")

end # module
