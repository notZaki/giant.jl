# Algorithm HALS

export halsW!, halsH!


function halsW!(M::AbstractMatrix{T},
                W::AbstractMatrix{T},
                H::AbstractMatrix{T},
                HMt::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                HHt::AbstractMatrix{T}=Matrix{T}(undef, 0, 0)) where T <: Real
    halsH!(M', H', W', HMt, HHt)
end


function halsH!(M::AbstractMatrix{T},
                W::AbstractMatrix{T},
                H::AbstractMatrix{T},
                WtM::AbstractMatrix{T}=Matrix{T}(undef, 0, 0),
                WtW::AbstractMatrix{T}=Matrix{T}(undef, 0, 0)) where T <: Real
    # Init
    r = size(H, 1)
    # If needed, compute intermediary values
    if length(WtM) == 0
        WtM = W' * M
    end
    if length(WtW) == 0
        WtW = W' * W
    end

    # Loop on rows of H
    for i in 1:r
        irowH = view(H, i, :)'
        deltaH = max.((WtM[i,:]' - WtW[i,:]' * H) / WtW[i,i], -irowH);
        irowH .= irowH .+ deltaH
        # Safety procedure
        for (idx, val) in enumerate(irowH)
            if !(val > 1e-14)
                irowH[idx] = 1e-14
            end
        end
    end
end
