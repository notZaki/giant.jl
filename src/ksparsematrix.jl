# arborescent for matrix-wise sparse MNNLS

export ksparsematrix

# Main function, builds cost matrix and calls assignement method
function ksparsematrix(M::AbstractMatrix{T},
                       W::AbstractMatrix{T},
                       targetk::Integer,
                       mink::Integer=1,
                       assignmethod::Function=assignglobal) where T <: Real
    # Init
    m, n = size(M)
    _, r = size(W)
    costmatrix = fill(Solution(Inf, [zero(T)]), r, n)

    WtW = W' * W
    WtM = W' * M

    # Perform homotopy for each column of H, and build cost matrix
    for j in 1:n
        sols, _ = arborescent(WtW, WtM[:,j], mink, returnpareto=true)
        for s in eachcol(sols)
            k = sum(s .> 0)
            err = norm(W*s - M[:,j]) ^ 2
            if costmatrix[k,j].err > err
                for i in k:r
                    costmatrix[i,j] = Solution(err, s)
                end
            end
        end
    end

    # Call assignement method and return
    H = assignmethod(costmatrix, targetk, M)
    return H
end
