# Testing Shamans on facial and hyperspectral image

using GIANT

using LinearAlgebra
using MAT
using Statistics

# Load data metadata and parameters
include("../xpmetadata.jl")


function runxp(p::XpParams, nbruns=10)
    # Announce
    println("\nProcessing $(uppercase(p.name))")

    # Read data and dictionnary
    data = matopen(p.datafile)
    M = Array(read(data, p.datavarname))
    dict = matopen(p.dictfile)
    W = Array(read(dict, p.dictvarname))

    # Transpose data if necessary
    if p.transposedata
        M = M'
    end
    if p.transposedict
        W = W'
    end

    # Get rank and sparsity target
    r = size(W,2)
    k = p.k
    matk = p.matk

    # display(size(M))
    # display(size(W))
    # display(k)
    # display(matk)

    # One run to compile all functions, get errors, sparsity and images
    # Run Homotopy no sparse (k=r)
    hnosH = homotopymatrix(M, W, r, assignksparse)
    hnoserr = norm(M - W * hnosH) / norm(M)
    hnosspar = colsparsity(hnosH)

    # Run Homotopy k-sparse
    hksH = homotopymatrix(M, W, k, assignksparse)
    hkserr = norm(M - W * hksH) / norm(M)
    hksspar = colsparsity(hksH)

    # Run Homotopy budget
    hbH = homotopymatrix(M, W, matk, assignglobal)
    hberr = norm(M - W * hbH) / norm(M)
    hbspar = colsparsity(hbH)

    # Save abundance maps
    npr = p.nbpixelsrow
    npc = p.nbpixelscol
    nir = p.nbimgperrow
    bw = p.bw
    displayabundancemap(hnosH, npr, npc, "out-$(p.name)-hnos.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(hksH, npr, npc, "out-$(p.name)-hks.png", bw=bw, nbimgperrow=nir)
    displayabundancemap(hbH, npr, npc, "out-$(p.name)-hb.png", bw=bw, nbimgperrow=nir)


    # Various runs to get better runtime estimation
    hnostime, hkstime, hgtime, hbtime = [zeros(nbruns) for _ in 1:4]
    for run in 1:nbruns
        print("$(run) ")
        # Run Homotopy no sparse (k=r)
        hnostime[run] = @elapsed hnosH = homotopymatrix(M, W, r, assignksparse)
        # Run Homotopy k-sparse
        hkstime[run] = @elapsed hksH = homotopymatrix(M, W, k, assignksparse)
        # Run Homotopy budget
        hbtime[run] = @elapsed hbH = homotopymatrix(M, W, matk, assignglobal)
    end

    println("")
    println("Algo\tHnos\tHks\tHb")
    println("Time\t$(round(median(hnostime),digits=2))\t$(round(median(hkstime),digits=2))\t$(round(median(hbtime),digits=2))")
    println("Error\t$(round(hnoserr*100,digits=2))\t$(round(hkserr*100,digits=2))\t$(round(hberr*100,digits=2))")
    println("Spar\t$(round(hnosspar,digits=2))\t$(round(hksspar,digits=2))\t$(round(hbspar,digits=2))")
end


runxp(paramcbcl)
runxp(paramfrey)
runxp(paramkuls)
runxp(paramjasper)
runxp(paramjasper_b)
runxp(paramsamson)
runxp(paramurban)
runxp(paramcuprite)
