
struct XpParams
    datafile::String    # name of the .mat file containing the data
    datavarname::String # name of the data matrix in the file
    transposedata::Bool # true if the data matrix is transposed
    dictfile::String    # name of the .mat file containing the dictionnary
    dictvarname::String # name of the dictionnary matrix in the file
    transposedict::Bool # true if the dictionnary matrix is transposed
    nbpixelsrow::Int    # for display, number of pixels per row in the image
    nbpixelscol::Int    # for display, number of pixels per column in the image
    nbimgperrow::Int    # display setup
    bw::Bool            # false to print white on black
    k::Int              # sparsity parameter for column-wise k-sparse algos
    matk::Int           # sparsity parameter for matrix-wise k-sparse algos
    name::String        # name of the dataset, to be used in results files
end



paramcbcl = XpParams("xp/data/cbclim.mat", "M", true,
                     "xp/data/cbclim_H.mat", "H", true,
                     19, 19, 7, false, 3, 3*361, "cbcl")

paramfrey = XpParams("xp/data/frey.mat", "M", true,
                     "xp/data/frey_H.mat", "H", true,
                     20, 28, 6, false, 6, 6*560, "frey")

paramkuls = XpParams("xp/data/kuls.mat", "M", true,
                     "xp/data/kuls_H.mat", "H", true,
                     64, 64, 5, false, 3, 3*4096, "kuls")

paramjasper = XpParams("xp/data/jasper.mat", "Y", false,
                       "xp/data/jasper-gt-r4.mat", "M", false,
                       100, 100, 4, false, 2, 20000, "jasper")

paramjasper_b = XpParams("xp/data/jasper.mat", "Y", false,
                       "xp/data/jasper-gt-r4.mat", "M", false,
                       100, 100, 4, false, 2, 18000, "jasper-k1.8")

paramsamson = XpParams("xp/data/samson.mat", "V", false,
                       "xp/data/samson-gt-r3.mat", "M", false,
                       95, 95, 3, false, 2, 2*9025, "samson")

paramsamson_b = XpParams("xp/data/samson.mat", "V", false,
                         "xp/data/samson-gt-r3.mat", "M", false,
                         95, 95, 3, false, 2, round(1.8*9025), "samson-k1.8")

paramurban = XpParams("xp/data/Urban.mat", "A", true,
                      "xp/data/Urban_Ref.mat", "References", true,
                      307, 307, 6, false, 2, 2*94249, "urban")

paramurban_b = XpParams("xp/data/Urban.mat", "A", true,
                        "xp/data/Urban_Ref.mat", "References", true,
                        307, 307, 6, false, 2, round(1.8*94249), "urban-k1.8")

paramcuprite = XpParams("xp/data/Cuprite.mat", "x", false,
                        "xp/data/Cuprite.mat", "U", false,
                        250, 191, 4, false, 4, 4*47750, "cuprite")

# TODO
# paramsandiego = XpParams("xp/data/SanDiego.mat", "A", true,
#                          "xp/data/TODO.mat", "TODO", false,
#                          400, 400, 5, false, 2, 2*16000, "sandiego")
