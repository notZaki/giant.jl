# Testing Brassens (sparse separable NMF)
# M  =  M(:,J)  *  H
# m*n   m*r        r*n and col-wise k-sparse

using GIANT
using LinearAlgebra

# Synthetic data parameters
m = 3
n = 20
r = 5
k = 2

# Algo parameters
deltastop = 1e-6
rmax = 4
targetk = 2

######################################################################
# Generate data

# Generate random W
realW = rand(m, r)
# Generate W with 1 interior point
# realW[:,4] .= rand()*realW[:,1] + rand()*realW[:,2] + rand()*realW[:,3]
# Nice handmade W
# realW = Float64[1 1 14 7 ; 1 12 1 6 ; 8 2 5 6]


# Generate H = [I,H'] with H' col-wise k-sparse
realH = rand_ksparse(r, n-r, k)
realH = hcat(I, realH) # I is the identity matrix from LinearAlgebra package

# Normalize (all columns sum to 1)
for col in eachcol(realH)
    col ./= sum(col)
end
for col in eachcol(realW)
    col ./= sum(col)
end

# Compute M
M = realW * realH

######################################################################
# Plots
using Plots

W = realW

# Draw simplex
plot([(1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 0, 0)], label="Unit simplex", camera=(80,45))
# Draw k-sparse hull
for j in 1:r
    for jj in j+1:r
        plot!([(W[1,j], W[2,j], W[3,j]) , (W[1,jj], W[2,jj], W[3,jj])], color=:orange, label="")
    end
end
# Draw points
scatter!(M[1,:], M[2,:], M[3,:], label="Data points")
scatter!(W[1,:], W[2,:], W[3,:], label="Vertices", markershape=:square)

gui()

######################################################################
# Tests

# # Run SNPA
# println("Run SNPA")
# @time snpaJ, _ = snpa(copy(M), rmax)
# # Given J, we recompute W and H with original M
# sort!(snpaJ)
# snpaW = M[:,snpaJ]
# snpaH = matrixactiveset(snpaW, M, sumtoone=true)
# display(snpaJ)


# Run Brassens
println("\nRun Brassens")
@time brassensJ, brassensH, extJ, candJ = brassens(copy(M), targetk, r, deltastop=deltastop)
sort!(brassensJ)
display(brassensJ)
println("Exterior vertices $(extJ)")
println("Interior vertices $(setdiff(brassensJ, extJ))")
println("Candidate vertices $(candJ)")
