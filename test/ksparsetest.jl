# Test for arborescent and bruteforce k-sparse NNLS

using GIANT
using LinearAlgebra

# Parameters
m = 100
n = 1
r = 20
k = 10

realW = rand(m, r)
realH = rand_ksparse(r, n, k)

# Normalize cols of H to unit l1-norm (to test sum-to-one constraint)
# for col in eachcol(realH)
#     col ./= sum(col)
# end

M = realW * realH


# Add noise to M
# noiseM = randn(m, n)
# noiseM ./= norm(noiseM)
# noiseM .*= norm(M) * 0.01
# M .+= noiseM
# M[M .< 0] .= 0 # Nonnegativity

# println("Real H")
# display(realH)

# Run once for compilation
# H = rand(r, n)
# ksparse_updtH!(M, realW, H, k, solver=bruteforce)
# ksparse_updtH!(M, realW, H, k)

println("\nCompute H with bruteforce")
bfH = rand(r, n)
@time bfnbnodes = ksparse_updtH!(copy(M), copy(realW), bfH, k, solver=bruteforce)
println("Error = $(norm(realH - bfH))")
println("$(bfnbnodes) nodes explored.")
# display(bfH)

println("\nCompute H with arborescent")
arboH = rand(r, n)
@time arbonbnodes = ksparse_updtH!(copy(M), copy(realW), arboH, k)
println("Error = $(norm(realH - arboH))")
println("$(arbonbnodes) nodes explored.")
# display(arboH)

# println("\nCompute H with arborescent sum-to-one")
# arboS1H = rand(r, n)
# @time arboS1nbnodes = ksparse_updtH!(copy(M), copy(realW), arboS1H, k, sumtoone=true)
# println("Error = $(norm(realH - arboS1H))")
# println("$(arboS1nbnodes) nodes explored.")
# # display(arboH)
